import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//creating and mapping our routes
import { AlbumsComponent } from "./components/albums/albums.component";
import {HomeComponent} from "./components/home/home.component";


const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'album', component: AlbumsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
