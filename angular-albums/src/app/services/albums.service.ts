import { Injectable } from '@angular/core';
import {Observable, of } from "rxjs";
import { Album } from '../models/Album';



@Injectable({
  providedIn: 'root'
})
export class AlbumsService {
  albums: Album[];

  data: Observable<any>;

  constructor() {
    this.albums = [
      {
        title: 'Changes',
        artist: 'Tupac',
        songs: ['Changes'],
        favorite: 'Changes',
        year: 1992,
        genre: 'Conscious hip hop',
        units: 6000000,
        image: "../../assets/image/2PacChanges.jpg",
        isActive: true
      },  //add the comma to add another array property

      {
        title: 'Brand New Man',
        artist: 'Brooks and Dunn',
        songs: ['Brand New Man', ' My Next Broken Heart ', ' Cool Drink of Water ', ' Cheating on the Blues ', ' Neon Moon ', ' Lost and Found ', ' I\'ve got a lot to Learn ', ' Boot Scootin\' Boogie ', ' Im No Good ', ' Still in Love with You '],
        favorite: 'Neon Moon',
        year: 1991,
        genre: 'Country',
        units: 6000000,
        image: "../../assets/image/BrooksAndDunn.jpg",
      },
      {
        title: 'The Emancipation of Mimi',
        artist: 'Mariah Carey',
        songs: [' It\'s Like That ', ' We Belong Together ', ' Shake It Off ', ' Mine Again ', 'Say Somethin\' ', ' Stay the Night ', ' Get Your Number ', ' One and Only ', ' Circles ', ' Your Girl ', ' I Wish You Knew ', ' To the Floor ', ' Joy Ride ', ' Fly Like a Bird '],
        favorite: 'We Belong Together',
        year: 2005,
        genre: 'Pop',
        units: 10000000,
        image: "../../assets/image/Mariah_Carey.png",
        isActive: true
      },
      {
        title: 'Confessions',
        artist: 'Usher',
        songs: ['Intro', ' Yeah! ', ' Throwback ', ' Confessions (Interlude) ', ' Confessions Part II ', ' Burn ', ' Caught Up ', ' SuperStar(Interlude) ', ' Superstar ', ' Truth Hurts ', ' Simple Things ', ' Bad Girl ', ' That\'s What It\'s Made For ', ' Can U Handle It? ', ' Do It to Me ', ' Take Your Hand ', ' Follow Me '],
        favorite: 'Burn',
        year: 2004,
        genre: 'string',
        units: 20000000,
        image: "../../assets/image/Usher.jpg",
      },
      {
        title: 'Songs in a Minor',
        artist: 'Alicia Keys',
        songs: [' Piano & I ', ' Girlfriend ', ' How Come You Don\'t Call Me ', ' Fallin ', ' Troubles ', ' Rock wit U ', ' A Woman\'s Worth ', ' Jane Doe ', ' Goodbye ', ' The Life ', ' Mr. Man ', ' Never Felt This Way ', ' Butterflyz ', ' Why Do I Feel So Sad ', ' Caged Bird ', ' Lovin U '],
        favorite: 'Fallin',
        year: 2001,
        genre: 'R&B',
        units: 16000000,
        image: "../../assets/image/AliciaKeys.jpg",
        isActive: true
      },
    ];
  //
  }// end of constructor

  getUsers(): Observable<Album[]> {
    // alert('Fetching users from service');
    console.log('Fetching users from service')
    return of(this.albums);
  }
// CREATE a method that adds a new user to the array//.push - is at the end of array
  addUser(user: Album) {
    console.log('Added user from service');
    this.albums.unshift(user);
  }



}//end of class don't delete
